require 'elasticsearch/model'

class User < ActiveRecord::Base
	
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

	has_many :books
  attr_accessor :remember_token
  before_save { self.email = email.downcase }
  
  mount_uploader :profile_picture, ProfilePictureUploader  
	REGEX_FOR_PHONE_NO = /\A[0-9]*\z/
	REGEX_FOR_EMAIL = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

	validates :name, presence: true , length: { maximum: 30 , minimum: 3}
	validates :phone_no,  presence: true , length: { maximum: 10 , minimum: 10 },
												format: {with: REGEX_FOR_PHONE_NO }, uniqueness: true
	validates :email , 	presence:true, length: {maximum:100, minimum:5},
											format: { with: REGEX_FOR_EMAIL }, uniqueness: {case_sensitive: false}
	validates :gender , presence:true, inclusion: { in: ["M", "F"] }
	validates :password, presence: true, length: { minimum:6 }, allow_nil: true
	validate :profile_picture_size 
	has_secure_password


	# Returns the hash digest of a given string
	def User.digest(string)
		cost = ActiveModel::SecurePassword::min_cost ? 	BCrypt::Engine::MIN_COST :
																										BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end
	# Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
  	 return false if remember_digest.nil?
     BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Definfing a proto-feed
  # Full in RailsTutorial Ch 12
  def feed
  	Book.where("user_id = ?", id)
  end

  private
  	def profile_picture_size
  		if profile_picture.size > 4.megabytes
  			errors.add(:profile_picture, " size should be less than 4MB")
			end  		
  	end
end

User.__elasticsearch__.create_index! force: true
User.import