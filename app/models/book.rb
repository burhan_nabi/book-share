require 'elasticsearch/model'

class Book < ActiveRecord::Base

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks


  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :name, presence: true, length: { maximum: 200 }
  validates :author, presence: true, length: {maximum: 200 }
  validates :semester, presence:true, numericality: true
  validates :price, presence:true, numericality: true
  validates :stream, presence:true
  validate :picture_size

  def as_indexed_json(options={})
    as_json(except: [:id, :_id])
  end

  private
  	def picture_size
  		if picture.size > 4.megabytes
	  		errors.add(:picture, " should be less than 4MB")
	  	end
  	end

  	def get_user
  		book.user
  	end
end

Book.__elasticsearch__.create_index! force: true
Book.import
