class SearchController < ApplicationController
	
	def search
		if params[:q].nil?
			@books = []
			@users = []
		else
			# @books = Book.search(params[:q]).records
			@books = Book.search(params[:q]).records
			# @books = Book.search(params[:q])			
			@users = User.search(params[:q]).records
		end
	end
end
