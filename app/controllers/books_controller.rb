class BooksController < ApplicationController

	before_action	:logged_in_user, only: [:create, :destroy, :new] 
	before_action :correct_user, only: :destroy

	def create
		@user = current_user
		@book = @user.books.build( book_params )
		if @book.save
			flash[:success] = "Book succesfully created!"
			redirect_to root_url
		else
			render new_book_path
		end
	end

	def destroy
		@book.destroy
		flash[:success] = "Book succesfully removed!"
		redirect_to request.referrer || root_url
	end

	def new
		@user = current_user
		@book = @user.books.build if logged_in?
	end

	private
		def book_params
			params.require(:book).permit(:name, :author, :semester, :price, :stream,
																	 :picture )
		end

		def correct_user
			@book = current_user.books.find_by(id: params[:id])
			redirect_to root_url if @book.nil?
		end
end
