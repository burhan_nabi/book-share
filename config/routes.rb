Rails.application.routes.draw do

	root 'static_pages#home'
	get 'signup' => 'users#new'
	get 'search' => 'search#search'	
	get 'login' 		=>	'sessions#new'
	post 'login'		=>	'sessions#create'
	delete 'logout' =>	'sessions#destroy'

	resources :users 

	resources :books, only: [:create, :destroy, :new]
end
