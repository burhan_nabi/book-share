require 'test_helper'

class BooksInterfaceTest < ActionDispatch::IntegrationTest

	def setup
		@user = users(:kshitiz)
	end

	test "books interface" do
	  log_in_as(@user)
	  get root_path
	  assert_select 'div.pagination'
	end

	test "invalid book submission" do
		log_in_as(@user)
		get new_book_path
		assert_select 'label', text: "Name"
		assert_no_difference	'Book.count' do
			post books_path, book: {
															name: ""
															}
			assert_select 'div#error_explanation'														
		end
	end

	test "valid book submission" do
	  log_in_as(@user)
	  get new_book_path
	  assert_select 'label', text: "Name"
	  name = "Harry Potter and the Deathly Hallows"
	  author = "J.K. Rowling"
	  stream = "ECE"
	  semester = 3
	  price = 400
	  assert_difference 'Book.count', 1 do
	  	post books_path, book: {
													  		name: name,
													  		author: author,
													  		stream: stream,
													  		semester: semester,
													  		price: price
													  	}
	 	end
	 	assert_redirected_to root_path
	 	follow_redirect!
	 	assert_match name, response.body
	end

	test "delete a book" do
	  log_in_as(@user)
	  get root_path
	  assert_select 'a', text: "Delete"
	  first_book = @user.books.paginate(page: 1).first
	  assert_difference 'Book.count', -1 do
	  	delete book_path(first_book)
	  end
	  assert_not flash.empty?
	end

	test "visiting a different user" do
	  get( user_path( users(:archer) ) )
	  assert_select 'a', text: "Delete", count: 0
	end
end

