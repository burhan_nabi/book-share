
require 'test_helper'

class UserTest < ActiveSupport::TestCase

	def	setup
		@user = User.new(	name:"Burhan Nabi", email:"burhan@nabi7.com", 
											phone_no:"7844444444", gender:"M",
											password:"password", password_confirmation:"password")
	end
	
	test "user's name should not be blank, should be of correct length" do
		
		assert @user.valid?	, "#{@user.errors.full_messages}"

		@user.name = ""
		assert_not @user.valid?
		
		@user.name = "r"*31
		assert_not @user.valid?
		
		@user.name = "r"*2
		assert_not @user.valid?
	end
	
	test "user's mobile number should not be blank, should be of correct length" do
		@user.phone_no = ""
		assert_not @user.valid?
		
		@user.phone_no = "2"*11
		assert_not @user.valid?
		
		@user.phone_no = "1"*9
		assert_not @user.valid?
	end
	
	test "Mobile no should be unique" do
		dup_user = @user.dup
		dup_user.phone_no = @user.phone_no
		@user.save
		assert_not dup_user.valid?
	end
	
	test "Mobile Number should consist of only digits" do
		
		assert @user.valid?,"#{@user.errors.full_messages}"
		
		@user.phone_no = "a"*10
		assert_not @user.valid?	
	end
	
	test "Gender Should be Valid" do
		assert @user.valid?,"#{@user.errors.full_messages}"
		
		@user.gender = "F"
		assert @user.valid?
		
		@user.gender = "L"
		assert_not @user.valid?
		
		@user.gender = "Male"
		assert_not @user.valid?
	end
	
	test "Email should of a minimum length, of the correct format" do
		assert @user.valid?,"#{@user.errors.full_messages}"
		
		@user.email = ""
		assert_not @user.valid?
		
		invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
		invalid_addresses.each do |invalid_add|
			@user.email = invalid_add
			assert_not @user.valid?, "#{invalid_add.inspect} should be invalid"
		end
	end
	
	test "Email should be unique" do
		dup_user = @user.dup
		dup_user.email = @user.email.upcase
		@user.save
		assert_not dup_user.valid?
	end
	
	test "Password should be present and have a minimum length" do
		@user.password = @user.password_confirmation = " "*6
		assert_not @user.valid?
		
		@user.password = @user.password_confirmation = "a"*5
		assert_not @user.valid?
	end
end
