require 'test_helper'

class BookTest < ActiveSupport::TestCase

	def setup
		@user = users(:kshitiz)
		@book = @user.books.build(	name:"HP and OoP", author:"JKR", semester: 1, price: 100,
											stream:"CSE/IT" )
	end

	test "should be valid" do
	  assert @book.valid?
	end

	test "user_id should be present" do
	  @book.user_id = nil
	  assert_not @book.valid?
	end

	test "name shuold be present" do
	  @book.name = " "
	  assert_not @book.valid?
	end

	test "book name should be of valid length" do
	  @book.name = "a"*201
	  assert_not @book.valid?
	end

	test "author shuold be present" do
	  @book.author = " "
	  assert_not @book.valid?
	end

	test "author name should be of valid length" do
	  @book.author = "a"*201
	  assert_not @book.valid?
	end

	test "semester should be present" do
	  @book.semester = nil
	  assert_not @book.valid?
	end

	test "price should should be present" do
	  @book.price = nil
	  assert_not @book.valid?	
	end

	test "stream should not be blank" do
		@book.stream = " "
		assert_not @book.valid?
	end

	test "order should be most recent first" do
	  assert_equal books(:most_recent), Book.first
	end
end
