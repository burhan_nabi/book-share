require 'test_helper'

class BooksControllerTest < ActionController::TestCase

	def setup
		@user = users(:kshitiz)
		@book = books(:harry_potter)
	end

	test "should redirect create when not logged in" do
	  assert_no_difference 'Book.count' do
	  	post :create, book: {
	  												name: "name",
	  												author: "author",
	  												semester: 1,
	  												price: 1,
	  												stream: "Btech",
	  												user: @user
	  											}
	  end
	  assert_redirected_to login_path
	end

	test "should redirect destroy when not logged in" do
	  assert_no_difference 'Book.count' do 
	  	delete :destroy, id: @book 
	  end
	  assert_redirected_to login_path
	end

	test "should redirect destroy for wrong book " do
	  log_in_as(users(:kshitiz))
	  book = books(:ants)
	  assert_no_difference 'Book.count' do
	  	delete :destroy, id: book
	  end
	  assert_redirected_to root_path
	end
end
